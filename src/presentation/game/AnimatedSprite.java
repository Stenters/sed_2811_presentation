/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Class that represents a simple animates sprite
 * Sprites are animated by having horizontally separated frames in a source image and the
 * displayed section changes over time
 */
public class AnimatedSprite extends Sprite {
    private double frameWidth;
    private double frameHeight;
    private int numFrames;
    private int frameTickCount;

    private int tickCount = 0;
    private int currentFrame = 0;

    /**
     * Constructor for Animated Sprite, calls the Sprite Constructor
     * @param owner what entity owns this sprite
     * @param depth what layer to render to, 0 renders first, high later
     * @param centered whether to render this sprite with the center as the origin, or the corner
     */
    public AnimatedSprite(GameEntity owner, int depth, boolean centered) {
        super(owner, depth, centered);
    }

    /**
     * Constructor for Animated Sprite, calls the Sprite Constructor
     * @param owner what entity owns this sprite
     * @param image image key for this sprite
     * @param depth what layer to render to, 0 renders first, high later
     * @param centered whether to render this sprite with the center as the origin, or the corner
     */
    public AnimatedSprite(GameEntity owner, String image, int depth, boolean centered) {
        super(owner, image, depth, centered);
    }

    /**
     * Method to configure the animation properties for this animate sprite
     * @param frameWidth the width of a single frame in pixels
     * @param frameHeight the height of a single frame in pixels
     * @param numFrames the number of frames in this animation
     * @param frameTickCount the number of ticks between each frame
     */
    public void configureAnimation(double frameWidth, double frameHeight,
                                   int numFrames, int frameTickCount){
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.numFrames = numFrames;
        this.frameTickCount = frameTickCount;
    }

    /**
     * Overrides the draw method to also select the appropriate frame for the current time
     * @param gc graphics context to render to
     */
    @Override
    public void draw(GraphicsContext gc){
        if(imageKey != null && images.containsKey(imageKey)) {
            Image image = images.get(imageKey);
            gc.setGlobalAlpha(alpha);
            double xOffset = frameWidth * currentFrame;
            if (centered) {
                gc.drawImage(image, xOffset, 0, frameWidth, frameHeight,
                        owner.getX() - frameWidth * .5,
                        owner.getY() - frameHeight * .5, frameWidth, frameHeight);
            } else {
                gc.drawImage(image, xOffset, 0, frameWidth, frameHeight,
                        owner.getX(),owner.getY(), frameWidth, frameHeight);
            }

            if (++tickCount >= frameTickCount){
                tickCount = 0;
                ++currentFrame;
            }
            if (currentFrame >= numFrames){
                tickCount = 0;
                currentFrame = 0;
            }
        }
    }
}
