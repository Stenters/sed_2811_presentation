/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

/**
 * Class for text to be overlaid on the screen
 */
public class HUDText extends HUDElement {

    private String text;
    private double x;
    private double y;
    private Paint color;
    private Font font;

    /**
     * Constructor that just sets values
     * @param text what text to display
     * @param x x position of the center of the text
     * @param y y position of the center of the text
     * @param color color of the text
     * @param font fond to render
     */
    public HUDText(String text, double x, double y, Paint color, Font font){
        super();
        this.text = text;
        this.x = x;
        this.y = y;
        this.color = color;
        this.font = font;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setFill(color);
        gc.setFont(font);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText(text, x, y);
    }
}
