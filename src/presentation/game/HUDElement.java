/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for a HUD element that will be drawn last
 */
public abstract class HUDElement {
    private static List<HUDElement> hudElements = new ArrayList<>();

    /**
     * constructor that just adds the element to the element list
     */
    public HUDElement(){
        hudElements.add(this);
    }

    /**
     * method that will draw the element to the screen
     * @param gc graphics context to render to
     */
    public abstract void draw(GraphicsContext gc);

    /**
     * removes the HUD element from the list
     */
    public void remove(){
        hudElements.remove(this);
    }

    /**
     * static method to draw all of the HUD elements
     * @param gc graphics context to render to
     */
    public static void drawAll(GraphicsContext gc){
        for (HUDElement e : hudElements){
            e.draw(gc);
        }
    }
}
