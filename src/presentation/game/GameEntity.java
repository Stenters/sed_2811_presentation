/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import java.util.ArrayList;
import java.util.List;

/**
 * interface for a generic game object that is updated every game tick
 */
public interface GameEntity {
    /**
     * Global list of entities that need to be updated
     * Not ideal, but this is a program to demonstrate the prototype pattern,
     * not game architecture design
     */
    List<GameEntity> livingEntities = new ArrayList<>();

    /**
     * Global list of entities to be added next iteration
     */
    List<GameEntity> newEntities = new ArrayList<>();

    /**
     * Global list of entities that are to be removed after the end of a tick
     */
    List<GameEntity> deadEntities = new ArrayList<>();

    /**
     * Loop callback function
     */
    void loop();

    /**
     * Cleanup method that should remove itself and any owned objects from the active lists
     */
    void kill();

    double getX();
    double getY();
}
