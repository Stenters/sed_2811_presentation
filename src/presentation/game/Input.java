/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import javafx.scene.Node;
import javafx.scene.input.KeyCode;

import java.util.HashMap;
import java.util.Map;

/**
 * Static class that manages user keyboard input
 */
public class Input {
    private static Map<Integer, Boolean> keyStates = new HashMap<>();

    /**
     * Configures the event handlers of the node
     * @param source what node to attach handlers to
     */
    public static void configureInput(Node source){
        source.setOnKeyPressed(keyEvent -> {
            int code = keyEvent.getCode().getCode();
            keyStates.put(code, true);
        });

        source.setOnKeyReleased(keyEvent -> {
            int code = keyEvent.getCode().getCode();
            keyStates.put(code, false);
        });
    }

    /**
     * Method to get the state of a key
     * @param code what key to get
     * @return true if key is pressed, false if key has not been seen yet or is not pressed
     */
    public static boolean getKey(KeyCode code){
        return keyStates.containsKey(code.getCode()) && keyStates.get(code.getCode());
    }

    /**
     * Method to get the state of a key
     * @param code what key to get
     * @return true if key is pressed, false if key has not been seen yet or is not pressed
     */
    public static boolean getKey(int code){
        return keyStates.containsKey(code) && keyStates.get(code);
    }
}
