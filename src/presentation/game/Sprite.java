/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class for an image that will be displayed on the canvas
 */
public class Sprite {
    public static TreeMap<Integer, List<Sprite>> activeImages = new TreeMap<>();

    protected static Map<String, Image> images = new HashMap<>();

    protected GameEntity owner;
    protected boolean centered;
    protected String imageKey;
    protected int depth;
    protected double alpha = 1.0;

    /**
     * Constructor for Sprite, configures values and activates the image
     * @param owner what entity owns this sprite
     * @param depth what layer to render to, 0 renders first, high later
     * @param centered whether to render this sprite with the center as the origin, or the corner
     */
    public Sprite(GameEntity owner, int depth, boolean centered){
        this.owner = owner;
        this.depth = depth;
        this.centered = centered;
        if(!activeImages.containsKey(depth)){
            activeImages.put(depth, new ArrayList<>());
        }
        activeImages.get(depth).add(this);
    }

    /**
     * Constructor for Sprite, also accepts an image key
     * @param owner what entity owns this sprite
     * @param image image key to initialize the sprite to
     * @param depth what layer to render to, 0 renders first, high later
     * @param centered wheter to render this sprite with the center as the origin, or the corner
     */
    public Sprite(GameEntity owner, String image, int depth, boolean centered){
        this(owner, depth, centered);
        this.imageKey = image;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public void setImageKey(String key){
        imageKey = key;
    }

    public Image getImage() {
        if (imageKey != null && images.containsKey(imageKey)) {
            return images.get(imageKey);
        }
        return null;
    }

    /**
     * Method for drawing the sprite to the canvas
     * @param gc graphics context to render to
     */
    public void draw(GraphicsContext gc){
        if(imageKey != null && images.containsKey(imageKey)) {
            Image image = images.get(imageKey);
            gc.setGlobalAlpha(alpha);
            if (centered) {
                gc.drawImage(image, owner.getX() - image.getWidth() * .5,
                        owner.getY() - image.getHeight() * .5);
            } else {
                gc.drawImage(image, owner.getX(),owner.getY());
            }
        }
    }

    /**
     * Cleanup method for the sprite
     */
    public void remove(){
        activeImages.get(depth).remove(this);
    }

    /**
     * Static method to register an image file to a given name
     * @param name name to be used as the image identifier
     * @param path path of the image
     */
    public static void registerImage(String name, String path){
        if(!images.containsKey(name)){
            images.put(name, new Image(path));
        }
    }

    /**
     * static method to draw all active sprites
     * @param gc graphics context to render to
     */
    public static void drawAll(GraphicsContext gc){
        for(List<Sprite> list : activeImages.values()){
            for(Sprite sprite : list){
                sprite.draw(gc);
            }
        }
    }
}
