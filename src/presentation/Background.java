/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation;

import javafx.scene.image.Image;
import presentation.game.GameEntity;
import presentation.game.Sprite;

/**
 * Class that represents the scrolling background in the game
 */
public class Background implements GameEntity {
    private static final int BASE_Y = -1000;
    private Sprite sprite;
    private double y = BASE_Y;

    /**
     * Constructor that initializes the sprites and activates the background
     */
    public Background(){
        sprite = new Sprite(this, "stars", 0, false);
        Sprite.registerImage("stars", "file:@../../res/images/background.png");
        newEntities.add(this);
    }

    @Override
    public double getX() {
        return 0;
    }

    @Override
    public double getY() {
        return y;
    }

    /**
     * Method that will shift the sprite over time and wrap the image to give the illusion of a
     * continuously moving background
     */
    @Override
    public void loop(){
        y += Controller.SCROLL_SPEED * Controller.LOOP_TIME;
        Image image = sprite.getImage();
        if (y > image.getHeight() * .5 + BASE_Y){
            y -= image.getHeight() * .5;
        }
    }

    /**
     * background should never die
     */
    @Override
    public void kill() {
        sprite.remove();
        deadEntities.add(this);
    }
}
