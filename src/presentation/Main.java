/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * main entrypoint for the application, launches the fx stuff
 */
public class Main extends Application {

    public static final int BASE_WIDTH = 800;
    public static final int BASE_HEIGHT = 800;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("canvasWindow.fxml"));
        primaryStage.setTitle("Space Battle");
        primaryStage.setScene(new Scene(root, BASE_WIDTH, BASE_HEIGHT));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
