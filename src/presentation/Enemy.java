/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       1/24/2019
 */
package presentation;

import presentation.game.CollisionBox;
import presentation.game.FileUtility;
import presentation.game.GameEntity;
import presentation.game.HealthBar;
import presentation.game.Sprite;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Class for defining the logic of an enemy
 */
public abstract class Enemy implements GameEntity {

    protected String ID;

    protected int maxHealth;
    protected int health;
    protected int def;
    protected int atk;
    protected int spd;
    protected int atkSpd;
    protected String imageName;

    protected double x;
    protected double y;

    protected boolean goingRight;

    protected Sprite sprite;
    protected CollisionBox collisionBox;
    protected HealthBar healthBar;

    /**
     * Empty constructor used for cloning
     */
    protected Enemy(){
    }

    /**
     * constructor that set the enemy position and adds it to the new entity list
     * @param x
     * @param y
     */
    public Enemy(double x, double y) {
        this.x = x;
        this.y = y;
        newEntities.add(this);
    }

    @Override
    public void kill() {
        sprite.remove();
        collisionBox.remove();
        healthBar.remove();
        deadEntities.add(this);
    }

    /**
     * method to deal damage to the enemy killing it if health reaches 0
     * @param amount amount of damage to do
     * @return true if dead, false otherwise
     */
    protected boolean damage(double amount) {
        health -= (amount - def);
        healthBar.setPercent((double) health / maxHealth);
        if (health <= 0) {
            kill();
            return true;
        }
        return false;
    }

    /**
     * Method to move the enemy left to right, reversing on the edges of the screen
     */
    protected void moveLeftToRight(){
        if(goingRight){
            x += spd * Controller.LOOP_TIME;
        } else {
            x -= spd * Controller.LOOP_TIME;
        }
        if (x + sprite.getImage().getWidth() * .5 > Controller.CANVAS_WIDTH){
            x = Controller.CANVAS_WIDTH - sprite.getImage().getWidth() * .5;
            goingRight = false;
        }
        if (x - sprite.getImage().getWidth() * .5 < 0){
            x = sprite.getImage().getWidth() * .5;
            goingRight = true;
        }
    }

    /**
     * Method to handle collisions with player bullets for a given collsion box
     * @param box what collision box to used (provided in the case of multiple collisionboxes)
     * @return true if this was killed
     */
    protected boolean handleCollisions(CollisionBox box){
        List<GameEntity> collidedBullets = box.getCollisions("playerBullets");
        boolean dead = false;
        if(collidedBullets != null){
            for(GameEntity entity : collidedBullets){
                if(entity instanceof Bullet){
                    dead = dead || damage(((Bullet) entity).getDamage());
                    entity.kill();
                }
            }
        }
        return dead;
    }

    /**
     * Method to read the basic stats from a given file
     * @param file file to read from
     * @return the data map that can have additional important values
     */
    protected Map<String, String> readStatsFromFile(File file) {
        Map<String, String> vals = FileUtility.loadDataFromFile(file);

        this.ID = vals.get("ID");
        this.maxHealth = Integer.parseInt(vals.get("Health"));
        String[] imageInfo = vals.get("Image").split(" ");
        Sprite.registerImage(imageInfo[0], imageInfo[1]);
        imageName = imageInfo[0];
        this.health = maxHealth;
        this.def = Integer.parseInt(vals.get("Def"));
        this.atk = Integer.parseInt(vals.get("Atk"));
        this.spd = Integer.parseInt(vals.get("Spd"));
        this.atkSpd = Integer.parseInt(vals.get("AtkSpd"));
        return vals;
    }
}