/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       1/24/2019
 */
package presentation;

import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import presentation.game.CollisionBox;
import presentation.game.GameEntity;
import presentation.game.HUDText;
import presentation.game.HealthBar;
import presentation.game.Sprite;

import java.io.File;
import java.util.Map;

/**
 * Class for defining Boss logic
 */
public class Boss extends Enemy implements GameEntity {
    private static int minionCounter;

    private Bullet prototypeBullet;
    private Minion prototypeMinion;

    private CollisionBox leftCollisionBox;
    private CollisionBox rightCollisionBox;

    private int minionCounterMax;

    private static final double HEALTHBAR_HEIGHT = 12;

    private static final double BULLET_XOFF = 65;
    private static final double BULLET_YOFF = 52;

    private int attackCounter = 0;

    /**
     * Constructor for the boss object, reads its stats from a file and initializes all of its game
     * components
     */
    public Boss(){
        super(0, 0);
        Map<String, String> dataValues =  readStatsFromFile(new File("file:@../../res/stats/bossStats"));

        x = Double.parseDouble(dataValues.get("BaseX"));
        y = Double.parseDouble(dataValues.get("BaseY"));

        double collisionXOffset = Double.parseDouble(dataValues.get("CollisionXOffset"));
        double collisionYOffset = Double.parseDouble(dataValues.get("CollisionYOffset"));
        double collisionRadius = Double.parseDouble(dataValues.get("CollisionRadius"));

        sprite = new Sprite(this, imageName, 8, true);
        collisionBox = new CollisionBox("boss", this, collisionRadius *  .25);
        leftCollisionBox = new CollisionBox("boss", this, collisionRadius);
        leftCollisionBox.setOffsets(-collisionXOffset, collisionYOffset);
        rightCollisionBox = new CollisionBox("boss", this, collisionRadius);
        rightCollisionBox.setOffsets(collisionXOffset, collisionYOffset);

        healthBar = new HealthBar(0, Controller.CANVAS_WIDTH, 0, HEALTHBAR_HEIGHT,
                Paint.valueOf("333333"), Paint.valueOf("AA0000"));

        prototypeBullet = new Bullet(new File("file:@../../res/stats/bossBullet"));
        prototypeBullet.setDamage(atk);

        prototypeMinion = new Minion(new File("file:@../../res/stats/minionStats"));

        minionCounterMax = Integer.parseInt(dataValues.get("MinionRate"));
        minionCounter = minionCounterMax;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void loop(){
        moveLeftToRight();
        handleCollisions(collisionBox);
        handleCollisions(leftCollisionBox);
        handleCollisions(rightCollisionBox);

        if(++attackCounter >= atkSpd){
            Bullet leftBullet = prototypeBullet.clone();
            Bullet rightBullet = prototypeBullet.clone();
            leftBullet.setPosition(x - BULLET_XOFF, y + BULLET_YOFF);
            rightBullet.setPosition(x + BULLET_XOFF, y + BULLET_YOFF);
            attackCounter = 0;
        }

        if (--minionCounter <= 0){
            spawnMinion();
            minionCounter = minionCounterMax;
        }

    }

    @Override
    public void kill(){
        super.kill();
        leftCollisionBox.remove();
        rightCollisionBox.remove();
        new HUDText("Victory", Controller.CANVAS_WIDTH * .5,
                Controller.CANVAS_HEIGHT * .5, Paint.valueOf("0055FF"), Font.font(96));
    }

    private void spawnMinion() {
        Minion newMinion = prototypeMinion.clone();
        newMinion.setPos(x, y + BULLET_YOFF);
    }
}