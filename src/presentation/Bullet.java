/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation;

import presentation.game.AnimatedSprite;
import presentation.game.CollisionBox;
import presentation.game.FileUtility;
import presentation.game.GameEntity;
import presentation.game.Sprite;

import java.io.File;
import java.util.Map;

/**
 * Class that represents a bullet that can be fired
 */
public class Bullet implements GameEntity, Prototype<Bullet> {
    private double x;
    private double y;
    private double angle;

    private double damage;
    private double speed;
    private String spriteName;
    private String collisionGroup;
    private double collisionRadius;
    private int numAnimationFrames;
    private int animationSpeed;

    private AnimatedSprite sprite;
    private CollisionBox collisionBox;


    /**
     * Empty constructor used by cloning
     */
    private Bullet(){
    }

    /**
     * Constructor for a bullet prototype
     * @param dataFile file to load the attributes from
     */
    public Bullet(File dataFile){
        Map<String, String> values = FileUtility.loadDataFromFile(dataFile);
        this.damage = Double.parseDouble(values.get("Damage"));
        this.speed = Double.parseDouble(values.get("Speed"));

        String[] spriteParts = values.get("Image").split(" ");
        this.spriteName = spriteParts[0];
        Sprite.registerImage(spriteParts[0], spriteParts[1]);

        this.collisionGroup = values.get("CollisionGroup");
        this.collisionRadius = Double.parseDouble(values.get("CollisionRadius"));

        this.numAnimationFrames = Integer.parseInt(values.get("AnimationFrames"));
        this.animationSpeed = Integer.parseInt(values.get("AnimationSpeed"));
    }

    public void setPosition(double x, double y){
        this.x = x;
        this.y = y;
    }

    public void setAngle(double angle){
        this.angle = angle;
    }

    public void setDamage(double damage){
        this.damage = damage;
    }

    public double getDamage(){
        return damage;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    /**
     * Method to move the bullet and kill it when it goes of the screen
     */
    @Override
    public void loop() {
        x += Math.sin(angle) * speed * Controller.LOOP_TIME;
        y += Math.cos(angle) * speed * Controller.LOOP_TIME;
        if(y + sprite.getImage().getHeight() * .5 < 0 ||
                y - sprite.getImage().getHeight() > Controller.CANVAS_HEIGHT){
            kill();
        }
    }

    /**
     * Cleans up the bullet
     */
    @Override
    public void kill() {
        sprite.remove();
        collisionBox.remove();
        deadEntities.add(this);
    }

    /**
     * Method to clone this bullet and initialize the sprite and collision data for it
     * @return the new bullet
     */
    @Override
    public Bullet clone() {
        Bullet newBullet = new Bullet();
        newBullet.damage = damage;
        newBullet.speed = speed;
        newBullet.animationSpeed = animationSpeed;
        newBullet.numAnimationFrames = numAnimationFrames;
        newBullet.collisionGroup = collisionGroup;
        newBullet.collisionRadius = collisionRadius;
        newBullet.spriteName = spriteName;

        newBullet.sprite = new AnimatedSprite(newBullet, spriteName, 2, true);
        newBullet.sprite.configureAnimation(newBullet.sprite.getImage().getHeight(),
                newBullet.sprite.getImage().getHeight(), numAnimationFrames, animationSpeed);
        newBullet.collisionBox = new CollisionBox(collisionGroup, newBullet, collisionRadius);
        newEntities.add(newBullet);
        return newBullet;
    }
}
