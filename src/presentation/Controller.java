/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import presentation.game.GameEntity;
import presentation.game.HUDElement;
import presentation.game.Input;
import presentation.game.Sprite;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller object for the fxml application, creates the game loop
 */
public class Controller implements Initializable {
    public static final double LOOP_TIME = 1 / 60.0;
    public static final double SCROLL_SPEED = 200;
    public static final double CANVAS_WIDTH = 500;
    public static final double CANVAS_HEIGHT = 500;

    @FXML
    private StackPane mainPane;

    @FXML
    private Canvas gameCanvas;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        mainPane.setStyle("-fx-background-color: #111111;");
        mainPane.widthProperty().addListener(((observableValue, oldValue, newValue) -> {
            resizeCanvas();
        }));
        mainPane.heightProperty().addListener(((observableValue, oldValue, newValue) -> {
            resizeCanvas();
        }));

        gameCanvas.setWidth(CANVAS_WIDTH);
        gameCanvas.setHeight(CANVAS_HEIGHT);
        gameCanvas.setFocusTraversable(true);

        Input.configureInput(gameCanvas);

        GraphicsContext gc = gameCanvas.getGraphicsContext2D();
        initGame();

        Timeline gameLoop = new Timeline(new KeyFrame(Duration.seconds(LOOP_TIME), actionEvent -> {
            GameEntity.livingEntities.addAll(GameEntity.newEntities);
            GameEntity.newEntities.clear();

            for(GameEntity entity : GameEntity.livingEntities){
                entity.loop();
            }

            GameEntity.livingEntities.removeAll(GameEntity.deadEntities);
            GameEntity.deadEntities.clear();

            gc.clearRect(0, 0, Main.BASE_WIDTH, Main.BASE_HEIGHT);
            Sprite.drawAll(gc);
            HUDElement.drawAll(gc);
        }));
        gameLoop.setCycleCount(Timeline.INDEFINITE);
        gameLoop.play();

    }

    private void initGame(){
        Player player = new Player();
        Background background = new Background();
        Boss boss = new Boss();
    }

    private void resizeCanvas(){
        double height = mainPane.heightProperty().getValue();
        double width = mainPane.widthProperty().getValue();
        if (height < width){
            gameCanvas.setScaleX(height / CANVAS_WIDTH);
            gameCanvas.setScaleY(height / CANVAS_HEIGHT);
        } else {
            gameCanvas.setScaleX(width / CANVAS_WIDTH);
            gameCanvas.setScaleY(width / CANVAS_HEIGHT);
        }
    }
}
