/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       1/24/2019
 */
package presentation;

import javafx.scene.paint.Paint;
import presentation.game.AnimatedSprite;
import presentation.game.CollisionBox;
import presentation.game.GameEntity;
import presentation.game.HealthBar;

import java.io.File;
import java.util.Map;

/**
 * Class for defining logic of a minion
 */
public class Minion extends Enemy implements GameEntity, Prototype<Minion> {
    private Paint foregroundColor;
    private Paint backgroundColor;
    private int healthBarHeight;
    private int healthBarWidth;
    private int healthBarOffset;

    private int numAnimationFrames;
    private int animationSpeed;

    private int attackCounter;
    private Bullet prototypeBullet;

    /**
     * Empty constructor used for cloning
     */
    private Minion(){
    }

    /**
     * Constructor for a minion from a file, prepares it to be used as a prototype
     * @param dataFile file to read from
     */
    public Minion(File dataFile){
        Map<String, String> data = readStatsFromFile(dataFile);
        foregroundColor = Paint.valueOf(data.get("HealthBarFg"));
        backgroundColor = Paint.valueOf(data.get("HealthBarBg"));
        healthBarWidth = Integer.parseInt(data.get("HealthBarWidth"));
        healthBarHeight = Integer.parseInt(data.get("HealthBarHeight"));
        healthBarOffset = Integer.parseInt(data.get("HealthBarOffset"));
        numAnimationFrames = Integer.parseInt(data.get("AnimationFrames"));
        animationSpeed = Integer.parseInt(data.get("AnimationSpeed"));
        prototypeBullet = new Bullet(new File("file:@../../res/stats/minionBullet"));
        prototypeBullet.setDamage(atk);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e){
            System.err.println("Minion woke up early");
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    public void setPos(double x, double y){
        this.x = x;
        this.y = y;
    }

    /**
     * Method to clone the minion, sets the new one's values and initializes its game elements
     * @return the new minion
     */
    @Override
    public Minion clone(){
        Minion newMinion = new Minion();
        newMinion.atk = atk;
        newMinion.spd = spd;
        newMinion.atkSpd = atkSpd;
        newMinion.health = health;
        newMinion.maxHealth = maxHealth;
        newMinion.def = def;
        newMinion.imageName = imageName;
        newMinion.foregroundColor = foregroundColor;
        newMinion.backgroundColor = backgroundColor;
        newMinion.healthBarHeight = healthBarHeight;
        newMinion.healthBarWidth = healthBarWidth;
        newMinion.healthBarOffset = healthBarOffset;

        newMinion.animationSpeed = animationSpeed;
        newMinion.numAnimationFrames = numAnimationFrames;

        newMinion.goingRight = Math.random() > .5;

        newMinion.prototypeBullet = prototypeBullet;

        newMinion.sprite = new AnimatedSprite(newMinion, imageName, 9, true);
        ((AnimatedSprite)newMinion.sprite).configureAnimation(
                newMinion.sprite.getImage().getHeight(), newMinion.sprite.getImage().getHeight(),
                numAnimationFrames, animationSpeed);
        newMinion.collisionBox = new CollisionBox("minions", newMinion,
                newMinion.sprite.getImage().getHeight() * .5);
        newMinion.healthBar = new HealthBar(0, healthBarWidth,
                0, healthBarHeight, backgroundColor, foregroundColor);
        newEntities.add(newMinion);
        return newMinion;
    }

    /**
     * Loop callback function
     */
    @Override
    public void loop() {
        move();
        if (handleCollisions(collisionBox)){
            deathBullets(5);
        }
        if (collisionBox.getCollisions("player") != null){
            deathBullets(7);
            kill();
        }
        if(++attackCounter >= atkSpd){
            prototypeBullet.clone().setPosition(x, y);
            attackCounter = 0;
        }
    }

    private void deathBullets(int count){
        for(int i = 0; i < count; ++i){
            Bullet deathBullet = prototypeBullet.clone();
            deathBullet.setPosition(x, y);
            deathBullet.setAngle(2 * 3.14 * ((double)i / count));
        }
    }

    private void move(){
        y += Controller.LOOP_TIME * spd * .35;
        moveLeftToRight();
        healthBar.setXY(x - healthBarWidth * .5, y + healthBarOffset);
        if (y > Controller.CANVAS_HEIGHT - 2 * sprite.getImage().getHeight()){
            deathBullets(50);
            kill();
        }
    }
}